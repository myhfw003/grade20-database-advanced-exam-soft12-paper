# 20级软件12班数据库高级期末考试试卷

## 考试要求：
1. 考试所用的部分表结构和数据已经提供（未提供的需要自行完成设计），以提供的表结构和数据为准
2. 建立自己的文件夹，自己的答题应全部位于自己的文件夹内（以姓名命名，找不到姓名的以0分计）
3. 不得直接修改本文件，可以在第二步创建的文件夹当中新建一个sql文件，题目复制到sql文件
4. 在SqlServer客户端中调试sql语句，将自己的答案填入第三步创建的sql文件当中每个题目的下方
5. 考试时间2个小时，试卷满分100分

```sql
一、单项选择题（ 0.5分1个,共25分，答案填写在括号内）

1．以下聚合函数求数据总和的是(      )

A．MAX

B．SUM

C．COUNT

D．AVG

2．可以用(      )来声明游标

A．CREATE CURSOR

B．ALTER CURSOR

C．SET CURSOR

D．DECLARE CURSOR


3．SELECT语句的完整语法较复杂，但至少包括的部分是(      )

A．仅SELECT

B．SELECT，FROM

C．SELECT，GROUP

D．SELECT，INTO


4．SQL语句中的条件用以下哪一项来表达(      )

A．THEN

B．WHILE

C．WHERE

D．IF


5．以下能够删除一列的是(      )

A．alter table emp remove addcolumn

B．alter table emp drop column addcolumn

C．alter table emp delete column addcolumn

D．alter table emp delete addcolumn


6．若要删除数据库中已经存在的表S，可用（ ）。

A．DELETE TABLE S

B．DELETE S

C．DROP S

D．DROP TABLE S


7．在视图上不能完成的操作是(      )

A．查询

B．在视图上定义新的视图

C．更新视图

D．在视图上定义新的表


8．UNIQUE惟一索引的作用是(      )

A．保证各行在该索引上的值都不得重复

B．保证各行在该索引上的值不得为NULL

C．保证参加惟一索引的各列，不得再参加其他的索引

D．保证惟一索引不能被删除


9．用于将事务处理写到数据库的命令是(      )

A．insert

B．rollback

C．commit

D．savepoint


10．在SQL语言中，子查询是（ ） 。

A．选取单表中字段子集的查询语句

B． 选取多表中字段子集的查询语句

C．返回单表中数据子集的查询语言

D．嵌入到另一个查询语句之中的查询语句


11．SQL语言的数据操纵语句包括SELECT、INSERT、UPDATE、DELETE等。其中最重要的，也是使用最频繁的语句是(      )。　

A．UPDATE

B．SELECT

C．DELETE

D．INSERT


12．SQL语言中，删除一个视图的命令是(      )

A．REMOVE

B．CLEAR

C．DELETE

D．DROP


13．修改数据库表结构用以下哪一项(      )

A．UPDATE

B．CREATE

C．UPDATED

D．ALTER


14．下列(      )不属于连接种类

A．左外连接

B．内连接

C．中间连接

D．交叉连接


15．若用如下的SQL语句创建了一个表SC：(      )

CREATE TABLE SC （S# CHAR（6） NOT NULL，C# CHAR（3） NOT NULL，SCORE INTEGER，NOTE CHAR（20））；
向SC表插入如下行时，（ ）行可以被插入 。

A．（NULL，’103’，80，’选修’）

B．（’200823’，’101’，NULL，NULL）

C．（’201132’，NULL，86，’ ’）

D．（’201009’，’111’，60，必修）



16．以下语句错误的是(      )

A． alter table emp delete column addcolumn;

B． alter table emp modify column addcolumn char(10);

C．alter table emp change addcolumn  addcolumn int;

D． alter table emp add column addcolumn int;


17．组合多条SQL查询语句形成组合查询的操作符是(      )

A．SELECT

B．ALL

C．LINK

D．UNION


18．以下哪项用来分组(      )

A．ORDER BY

B．ORDERED BY

C．GROUP BY

D．GROUPED BY


19．若要在基本表S中增加一列CN（课程名），可用(      )

A．ADD TABLE S ALTER（CN CHAR（８））

B．ALTER TABLE S ADD（CN CHAR（８））

C．ADD TABLE S（CN CHAR（８））

D．ALTER TABLE S （ADD CN CHAR（８））


20．下列的SQL语句中，(      )不是数据定义语句。

A．CREATE TABLE

B．GRANT

C．CREATE VIEW

D． DROP VIEW


21．删除经销商1018的数据记录的代码为(      ) from distributors where distri_num=1018

A．drop table

B．delete *

C．drop column

D．delete


22．按照姓名降序排列(      )

A．ORDER BY DESC NAME

B．ORDER BY NAME DESC

C．ORDER BY NAME ASC

D．ORDER BY  ASC NAME


23．可以在创建表时用(      )来创建唯一索引，也可以用(      )来创建唯一索引

A．Create table，Create index

B．设置主键约束，设置唯一约束

C．设置主键约束，Create index

D．以上都可以


24．在SELECT语句中，使用关键字(      )可以把重复行屏蔽

A．TOP

B．ALL

C．UNION

D．DISTINCT


25．以下聚合函数求平均数的是(      )

A．COUNT

B．MAX

C．AVG

D．SUM


26．返回当前日期的函数是(      )

A．curtime()

B．adddate()

C．curnow()

D．getdate()



27．SELECT COUNT(SAL) FROM EMP GROUP BY DEPTNO;意思是(      )

A．求每个部门中的工资

B．求每个部门中工资的大小

C．求每个部门中工资的综合

D．求每个部门中工资的个数


28．从GROUP BY分组的结果集中再次用条件表达式进行筛选的子句是(      )

A．FROM

B．ORDER BY

C．HAVING

D．WHERE


29．为数据表创建索引的目的是(      )

A．提高查询的检索性能

B．归类

C．创建唯一索引

D．创建主键


30．如果要回滚一个事务，则要使用(      )语句。

A．commit   transaction

B． begin   transaction

C． revoke   transaction   

D．rollback   transaction   


31．触发器不是响应以下哪一语句而自动执行的sql语句

A．select

B．insert

C．delete

D．update


32．(      )表示一个新的事务处理块的开始

A．START TRAN

B．BEGIN TRAN

C．BEGIN COMMIT

D．START COMMIT


33．以下语句不正确的是(      )

A．select * from emp;

B．select ename,hiredate,sal from emp;

C．select * from emp order deptno;

D．select * from where deptno=1 and sal<300;


34．例如数据库中有A表，包括学生，学科，成绩 ，序号四个字段 , 数据库结构为

学生     学科     成绩  序号

张三     语文     60    1

张三     数学     100   2

李四     语文     70    3

李四     数学     80    4

李四     英语     80    5

上述哪一列可作为主键列(      )

A．序号

B．成绩

C．学科

D．学生


35．学生关系模式 S（ S＃，Sname，Sex，Age），S的属性分别表示学生的学号、姓名、性别、年龄。要在表S中删除一个属性“年龄”，可选用的SQL语句是（ ）。

A． UPDATE S Age

B．DELETE Age from S

C．ALTER TABLE S ‘Age’

D． ALTER TABLE S DROP Age


36．以下哪项用于左连接(      )

A．JOIN

B．RIGHT JOIN

C．LEFT JOIN

D．INNER JOIN


37．一张表的主键个数为(      )

A．至多3个

B．没有限制

C．至多1个

D．至多2个


38．以下表示可变长度字符串的数据类型是(      )

A．TEXT

B．CHAR

C．VARCHAR

D．EMUM


39．以下说法错误的是(      )

A．SELECT max(sal),deptno,job FROM EMP group by sal;

B．SELECT max(sal),deptno,job FROM EMP group by deptno;

C．SELECT max(sal),deptno,job FROM EMP;

D．SELECT max(sal),deptno,job FROM EMP group by job;


40．创建视图的命令是(      )

A．alter view

B．alter table

C．create table

D．create view


41．存储过程是一组预先定义并(      )的Transact-SQL语句

A．保存

B．编写

C．编译

D．解释


42．SQL语言集数据查询、数据操纵、数据定义和数据控制功能于一体，其中，CREATE、DROP、ALTER语句是实现哪种功能(      )

A．数据操纵

B．数据控制

C．数据定义

D．数据查询


43．以下哪项不属于DML操作(      )

A．insert

B．update

C．delete

D．create


44．有关系S（S＃，SNAME，SAGE），C（C＃，CNAME），SC（S＃，C＃，GRADE）。其中S＃是学生号，SNAME是学生姓名，SAGE是学生年龄， C＃是课程号，CNAME是课程名称。要查询选修“ACCESS”课的年龄不小于20的全体学生姓名的SQL语句是SELECT SNAME FROM S，C，SC WHERE子句。这里的WHERE子句的内容是（ ）。

A．SAGE>=20 and CNAME=’ ACCESS’

B．S.S# = SC.S# and C.C# = SC.C# and SAGE in>=20 and CNAME in ‘ACCESS’

C．SAGE in>=20 and CNAME in ‘ACCESS’

D．S.S# = SC.S# and C.C# = SC.C# and SAGE>=20 and CNAME=‘ACCESS’


45．以下哪项属于DDL操作(      )

A．update

B．create

C． insert

D．delete


46．条件“IN(20,30,40)”表示(      )

A．年龄在20到40之间

B．年龄在20到30之间

C．年龄是20或30或40

D．年龄在30到40之间


47．关系数据库中，主键是(      )

A．创建唯一的索引，允许空值

B．只允许以表中第一字段建立

C．允许有多个主键的

D．为标识表中唯一的实体


48．例如数据库中有A表，包括学生，学科，成绩三个字段 , 数据库结构为

学生     学科     成绩

张三     语文     80

张三     数学     100

李四     语文     70

李四     数学     80

李四     英语     80

如何统计每个学科的最高分(      )

A．select 学生,max(成绩) from A group by 学生;

B．select 学科,max(成绩) from A group by 学科;

C．select 学生,max(成绩) from A order by学生;

D．select 学生,max(成绩) from A group by 成绩;


49．数据库服务器、数据库和表的关系，正确的说法是(      )

A．一个数据库服务器只能管理一个数据库，一个数据库只能包含一个表

B．一个数据库服务器可以管理多个数据库，一个数据库可以包含多个表

C．一个数据库服务器只能管理一个数据库，一个数据库可以包含多个表

D．一个数据库服务器可以管理多个数据库，一个数据库只能包含一个表


50．例如数据库中有A表，包括学生，学科，成绩三个字段 , 数据库结构为

学生     学科     成绩

张三     语文     60

张三     数学     100

李四     语文     70

李四     数学     80

李四     英语     80

如何统计最高分>80的学科(      )

A．SELECT MAX(成绩)  FROM A GROUP BY学科  HAVING MAX(成绩)>80;

B．SELECT学科  FROM A GROUP BY学科  HAVING成绩>80;

C．SELECT学科  FROM A GROUP BY学科  HAVING MAX(成绩)>80;

D．SELECT学科  FROM A GROUP BY学科 WHERE MAX(成绩)>80;



二、综合题

1. 描述（10分）
针对如下表Author结构创建索引，写出创建索引的SQL语句：

CREATE TABLE Author 
(
   Id  int  NOT NULL PRIMARY KEY identity,
   FirstName  nvarchar(45) NOT NULL,
   LastName  nvarchar(45) NOT NULL,
   UpdatedTime  datetime NOT NULL
 )

对FirstName创建唯一索引uniq_idx_firstname，对LastName创建普通索引idx_lastname

2. 描述（15分）
构造一个触发器trg_AuditLog，在向Employees表中插入一条数据的时候，触发插入相关的数据到AuditLog中。

-- 职员表
CREATE TABLE Employees
(
   Id INT PRIMARY KEY NOT NULL identity,
   Name nvarchar(80) NOT NULL,
   Age INT NOT NULL,
   Address NVARCHAR(50),
   SALARY decimal(18,2)
);
-- 审计日志表
CREATE TABLE AuditLog
(
   Id int primary key not null identity,
   NAME TEXT NOT NULL,
   Salary decimal(18,2)
);

后台会往Employees插入一条数据:
INSERT INTO Employees (NAME,AGE,ADDRESS,SALARY)VALUES ('Paul', 32, 'California', 20000.00 );
然后从AuditLog里面使用查询语句:
select * from AuditLog;

输出：
Paul | 20000.00


3. 描述（10分）
针对Author表创建视图vw_Author，只包含FirstName以及LastName两列，并对这两列重新命名，FirstName为v_FirstName，LastName修改为v_LastName：

CREATE TABLE Author 
(
   Id  int  NOT NULL PRIMARY KEY identity,
   FirstName  nvarchar(45) NOT NULL,
   LastName  nvarchar(45) NOT NULL,
   UpdatedTime  datetime NOT NULL
)
后台会插入2条数据:
insert into Author (FirstName,LastName,UpdatedTime) values ('PENELOPE', 'GUINESS', '2006-02-15 12:34:33'), ('NICK', 'WAHLBERG', '2006-02-15 12:34:33');

查询视图：select * from vw_Author;
输出：
['first_name_v', 'last_name_v']
PENELOPE|GUINESS
NICK|WAHLBERG


4. 描述（10分）
分页查询Employees表，每5行一页，返回第2页的数据

CREATE TABLE Employees
(
   Id int primary key NOT NULL identity,
   EmployeeCode nvarchar(80),
   Birthday date NOT NULL,
   FirstName nvarchar(14) NOT NULL,
   LastName nvarchar(16) NOT NULL,
   Gender char(1) NOT NULL,
   HireDate date NOT NULL
);
插入一些数据：
INSERT INTO employees (EmployeeCode,Birthday,FirstName,LastName,Gender,HireDate)  VALUES('10001','1953-09-02','Georgi','Facello','M','1986-06-26');
INSERT INTO employees (EmployeeCode,Birthday,FirstName,LastName,Gender,HireDate)  VALUES('10002','1964-06-02','Bezalel','Simmel','F','1985-11-21');
INSERT INTO employees (EmployeeCode,Birthday,FirstName,LastName,Gender,HireDate)  VALUES('10003','1959-12-03','Parto','Bamford','M','1986-08-28');
INSERT INTO employees (EmployeeCode,Birthday,FirstName,LastName,Gender,HireDate)  VALUES('10004','1954-05-01','Chirstian','Koblick','M','1986-12-01');
INSERT INTO employees (EmployeeCode,Birthday,FirstName,LastName,Gender,HireDate)  VALUES('10005','1955-01-21','Kyoichi','Maliniak','M','1989-09-12');
INSERT INTO employees (EmployeeCode,Birthday,FirstName,LastName,Gender,HireDate)  VALUES('10006','1953-04-20','Anneke','Preusig','F','1989-06-02');
INSERT INTO employees (EmployeeCode,Birthday,FirstName,LastName,Gender,HireDate)  VALUES('10007','1957-05-23','Tzvetan','Zielinski','F','1989-02-10');
INSERT INTO employees (EmployeeCode,Birthday,FirstName,LastName,Gender,HireDate)  VALUES('10008','1958-02-19','Saniya','Kalloufi','M','1994-09-15');
INSERT INTO employees (EmployeeCode,Birthday,FirstName,LastName,Gender,HireDate)  VALUES('10009','1952-04-19','Sumant','Peac','F','1985-02-18');
INSERT INTO employees (EmployeeCode,Birthday,FirstName,LastName,Gender,HireDate)  VALUES('10010','1963-06-01','Duangkaew','Piveteau','F','1989-08-24');
INSERT INTO employees (EmployeeCode,Birthday,FirstName,LastName,Gender,HireDate)  VALUES('10011','1953-11-07','Mary','Sluis','F','1990-01-22');

查询（5行每页）第2页的记录，输出：
6|10006|1953-04-20|Anneke|Preusig|F|1989-06-02
7|10007|1957-05-23|Tzvetan|Zielinski|F|1989-02-10
8|10008|1958-02-19|Saniya|Kalloufi|M|1994-09-15
9|10009|1952-04-19|Sumant|Peac|F|1985-02-18
10|10010|1963-06-01|Duangkaew|Piveteau|F|1989-08-24

5. 描述（30分）
开发组收到M公司的开发邀约，拟开发一套OA办公自动化系统，该系统的部分功能及初步需求分析的结果如下 ：

（1）M公司旗下有业务部、策划部和其他部门。部门信息包括部门号、部门名、主管、联系电话和邮箱号；每个部门只有一名主管，只负责管理本部门的工作，且主管参照员工关系的员工号；一个部门有多名员工，每名员工属于且仅属于一个部门。

（2）员工信息包括员工号、姓名、职位、联系方式和薪资。职位包括主管、业务员、 策划员等。业务员负责受理用户申请，设置受理标志。一名业务员可以受理多个用户申请，但一个用户申请只能由一名业务员受理。

（3）用户信息包括用户号、用户名、银行账号、电话、联系地址。用户号唯一标识用户信息中的每一个元组。

（4）用户申请信息包括申请号、用户号、会议日期、天数、参会人数、地点、预算和受理标志（受理标志有：已申请（默认）、已拒绝、已受理、延期受理、已过期）。申请号唯一标识用户申请信息中的每一个元组，且一个用户可以提交多个申请，但一个用户申请只对应一个用户号。

（5）策划部主管为己受理的用户申请制定会议策划任务。策划任务包括申请号、任务明细和要求完成时间.申请号唯一标识策划任务的每一个元组。一个策划任务只对应一个已受理的用户申请，但一个策划任务可由多名策划员参与执行，且一名策划员可以参与执行，且修改策划任务。

題目1（10分）：使用PowerDesigner，设计可以使用的SqlServer数据库物理模型，要求中文名称和英文名称清楚（英文不知道的可以借助翻译软件），表关系准确；利用设计好的模型生成sql语句，创建一个名为OADatabase的数据库，备用；
题目2（10分）：为了保证当用户申请会议，受理标志为 已申请，请设计一个触发器，当用户申请信息表插入数据的时候，可以判断其默认受理标志是否为：已申请，如果是，则无需处理允许正常插入，如果不是则重新个性受理标志为：已申请；
题目3（10分）：当业务员受理用户申请的时候，需要将当前处理的记录中受理标志修改为：已受理，同时将其它所有状态为：已申请，且会议日期已超过的记录中的受理标志个性为：已过期，请设计一个存储过程来处理此业务；

```